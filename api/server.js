// MARK: --- REQUIRE MODULES
const express = require('express')
const server = express()

const morgan = require('morgan')

const mySqlConnection = require('./api/databaseHelpers/mySqlWrapper')
const userDBHelper = require('./api/databaseHelpers/userDBHelper')(mySqlConnection)

const accessTokenDBHelper = require('./api/databaseHelpers/accessTokensDBHelper')(mySqlConnection)
const oAuthModel = require('./api/authorisation/accessTokenModel')(userDBHelper, accessTokenDBHelper)
const oAuth2Server = require('node-oauth2-server/')

server.oauth = oAuth2Server({
  model: oAuthModel,
  grants: ['password', 'client_credentials'],
  debug: true,
  accessTokenLifetime: 60 * 60
})

const restrictedAreaRoutesMethods = require('./api/restrictedArea/restrictedAreaRoutesMethods.js')
const restrictedAreaRoutes = require('./api/restrictedArea/restrictedAreaRoutes.js')(express.Router(), server, restrictedAreaRoutesMethods)
const authRoutesMethods = require('./api/authorisation/authRoutesMethods')(userDBHelper)
const authRoutes = require('./api/routes/auth')(express.Router(), server, authRoutesMethods)
const bodyParser = require('body-parser')

server.use(morgan('dev'))
server.use(bodyParser.urlencoded({ extended: false }))
server.use(bodyParser.json())

server.use(server.oauth.errorHandler())

server.use('/auth', authRoutes)

server.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  )

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET')
    return res.status(200).json({})
  }

  next()

})

server.use((req, res, next) => {
  const error = new Error('Not found')
  error.status = 404
  next(error)
})

server.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: {
      message: error.message
    }
  })
})

server.get('/foo', function(req, res){
  console.log('Hello, I am foo.');
});

module.exports = server
