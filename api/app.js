var fs = require('fs');
var http = require('http');
var https = require('https');

var privateKey  = fs.readFileSync('certificate.key', 'utf8');
var certificate = fs.readFileSync('certificate.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

const server = require('./server')

const dotenv = require('dotenv')
dotenv.config()
const port = process.env.PORT || 3000

const serverapi = http.createServer(server)

serverapi.listen(port, () => {
  console.log(`listening on port ${port}`)
})

var httpsServer = https.createServer(credentials, server);
httpsServer.listen(8443, () => {
  console.log(`listening on port 8443`)
})