module.exports = (router, server, authRoutesMethods) => {
  // route for registering new users
  router.post('/registerUser', authRoutesMethods.registerUser)

  // route for allowing existing users to login
  router.post('/login', server.oauth.grant())
  return router

}
