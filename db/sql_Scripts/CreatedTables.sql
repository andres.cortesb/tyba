CREATE TABLE apps (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(128) UNIQUE NOT NULL,
  description text,
  state boolean DEFAULT 0
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE complements (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  type integer UNIQUE,
  name varchar(128) UNIQUE NOT NULL,
  description text,
  state boolean DEFAULT 0
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE types (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(128) UNIQUE NOT NULL,
  description text,
  state boolean DEFAULT 0
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE users (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username varchar(128) UNIQUE NOT NULL,
  email varchar(255) UNIQUE NOT NULL,
  password varchar(255) NOT NULL,
  cellphone varchar(10) NOT NULL,
  state boolean DEFAULT 0,
  created_at timestamp DEFAULT CURRENT_TIMESTAMP,
  modified_at datetime
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE users_access_tokens (
  access_token text,
  client_id text,
  user_id integer UNIQUE,
  expires timestamp
);

CREATE TABLE apps_users (
  id_app integer UNIQUE,
  id_user integer UNIQUE,
  api_key varchar(255) UNIQUE NOT NULL
);

CREATE TABLE apps_complements (
  id_app integer UNIQUE,
  id_component integer UNIQUE
);

ALTER TABLE complements ADD FOREIGN KEY (`type`) REFERENCES types (`id`);
ALTER TABLE users_access_tokens ADD FOREIGN KEY (`user_id`) REFERENCES users (`id`);
ALTER TABLE apps_users ADD FOREIGN KEY (`id_app`) REFERENCES apps (`id`);
ALTER TABLE apps_users ADD FOREIGN KEY (`id_user`) REFERENCES users (`id`);
ALTER TABLE apps_complements ADD FOREIGN KEY (`id_app`) REFERENCES apps (`id`);
ALTER TABLE apps_complements ADD FOREIGN KEY (`id_component`) REFERENCES complements (`id`);
